## [Unreleased]
### Added
- Added misc. ui stuff. 
- Added disabling/enabling login button based on input.

### Changed
- Changed appearance of certain buttons.
- Changed appearance of views of different screen size.

## [0.3.0] - 2019-10-01
### Added
- Deletion of user realm data upon logout.

### Changed
- Made error messages more human readable.

## [0.2.0] - 2019-09-25
### Added
- Checking of session expiration.

## [0.1.2] - 2019-09-24
### Changed
- Changed platform text field in store / update trainee to platform picker.

## [0.1.1] - 2019-09-23
### Fixed
- Fixed issue with trainee transactions.

## [0.1.0] - 2019-09-20
### Added
- Logging in using the created account.
- Viewing of the stored trainees.
- Storing of new trainees.
- Updating and deleting stored trainees.
