//
//  FieldValidations.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 15/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct FieldValidations {
  public static func validate(input: String, fieldType: FieldTypes, isRequired: Bool) -> (isValid: Bool, message: String) {
    var validFlag: Bool = true
    var errorMessage: String = ""
    
    if input.isEmpty && isRequired {
      validFlag = false
      errorMessage = "\(fieldType.type) must not be empty."
    } else if !matchString(string: input, pattern: fieldType.pattern) {
      validFlag = false
      errorMessage = fieldType.errorMessage
    }
    
    return (validFlag, errorMessage)
  }
  
  private static func matchString(string: String, pattern: String) -> Bool {
    var validFlag: Bool = true
    var regex: NSRegularExpression?
    
    try? regex = NSRegularExpression(pattern: pattern)
    
    if regex != nil {
      validFlag = regex!.firstMatch(
        in: string,
        options: [],
        range: NSRange(location: 0, length: string.utf16.count)) != nil
    } else {
      validFlag = false
    }
    
    return validFlag
  }
}

enum FieldTypes {
  case name
  case email
  case password
  
  var type: String {
    switch self {
    case .name:
      return "Name"
    case .email:
      return "Email"
    case .password:
      return "Password"
    }
  }
  
  var pattern: String {
    switch self {
    case .name:
      return #"^[A-Za-z0-9\ ]+$"#
    case .email:
      return #"^[A-Za-z0-9\_\.]+\@[A-Za-z0-9]+\.[A-Za-z]{1,4}(\.[A-Za-z]{1,3}){0,1}$"#
    case .password:
      return #".+"#
    }
  }
  
  var errorMessage: String {
    switch self {
    case .name:
      return "Name must be characters only."
    case .email:
      return "Invalid Email format."
    case .password:
      return "Invalid password"
    }
  }
}
