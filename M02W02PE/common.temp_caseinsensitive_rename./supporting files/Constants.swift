//
//  Constants.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 07/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct Constants {
  static let baseUrl = "https://wc-training.johnerisvillanueva.com"
  
  enum HttpHeaderField: String {
    case authorization = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
  }
  
  enum ContentType: String {
    case json = "application/json"
  }
  
  static let platforms = ["mobile", "backend", "frontend"]
}
