//
//  TraineeTableViewCell.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

class TraineeTableViewCell: UITableViewCell {
  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var traineeImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
}
