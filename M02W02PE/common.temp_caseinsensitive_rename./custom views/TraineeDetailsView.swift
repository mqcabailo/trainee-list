//
//  TraineeDetailsView.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 15/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

@IBDesignable
class TraineeDetailsView: UIView {
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var platformTextField: UITextField!
  @IBOutlet weak var endButton: UIButton!
  @IBInspectable var endButtonText: String = "" {
    didSet {
      endButton.setTitle(endButtonText, for: .normal)
    }
  }
  var endButtonEvent: UITapGestureRecognizer = UITapGestureRecognizer(target: nil, action: nil) {
    didSet {
      endButton.addGestureRecognizer(endButtonEvent)
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    let bundle = Bundle(for: TraineeDetailsView.self)
    bundle.loadNibNamed("TraineeDetailsView", owner: self, options: nil)
    addSubview(contentView)
    contentView.frame = self.bounds
    contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    
    nameTextField.delegate = self
    emailTextField.delegate = self
    platformTextField.delegate = self
    platformTextField.text = Constants.platforms.first
  }
  
  private func showPlatformPicker() {
    ActionSheetStringPicker.show(
      withTitle: "Platform",
      rows: Constants.platforms,
      initialSelection: 0,
      doneBlock: { (_, _, value) in
        if let value = value {
          self.platformTextField.text = (value as? String)
        }
    },
      cancel: { (_) in
        return
    },
      origin: self.contentView)
  }
}

extension TraineeDetailsView: UITextFieldDelegate {
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == platformTextField {
      print(textField.tag)
      showPlatformPicker()
      textField.resignFirstResponder()
      return false
    }
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
      nextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
    }
    return false
  }
}
