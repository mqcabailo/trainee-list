//
//  RoundedUIButton.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedUIButton: UIButton {
  @IBInspectable var cornerRadius: CGFloat = 2 {
    didSet {
      layer.cornerRadius = frame.size.height / cornerRadius
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    
  }
}
