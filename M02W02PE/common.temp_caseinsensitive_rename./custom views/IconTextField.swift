//
//  IconTextField.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 04/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

@IBDesignable
class IconTextField: UITextField {
  @IBInspectable var leftIcon: UIImage? = nil {
    didSet {
      let iconView = UIImageView(frame:
        CGRect(x: 10, y: 5, width: 20, height: 20))
      iconView.image = leftIcon
      let iconContainerView: UIView = UIView(frame:
        CGRect(x: 20, y: 0, width: 30, height: 30))
      iconContainerView.addSubview(iconView)
      leftView = iconContainerView
      leftViewMode = .always
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
}
