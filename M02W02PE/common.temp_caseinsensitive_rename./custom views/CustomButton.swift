//
//  CustomButton.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 04/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
  @IBInspectable var cornerRadius: CGFloat = 2 {
    didSet {
      layer.cornerRadius = frame.size.height / cornerRadius
    }
  }
  
  @IBInspectable var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }
  
  @IBInspectable var borderColor: UIColor? = nil {
    didSet {
      layer.borderColor = borderColor?.cgColor
    }
  }
  
  @IBInspectable var borderOpacity: CGFloat = 1 {
    didSet {
      //layer.opacity
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    
  }
}
