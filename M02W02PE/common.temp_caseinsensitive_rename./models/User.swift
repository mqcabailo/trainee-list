//
//  User.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 15/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct User {
  public let email: String
  public let password: String
}

// MARK: - Encodable
extension User: Encodable {
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(email, forKey: .email)
    try container.encode(password, forKey: .password)
  }
}

// MARK: - Coding Keys
extension User {
  private enum CodingKeys: String, CodingKey {
    case email = "email"
    case password = "password"
  }
}
