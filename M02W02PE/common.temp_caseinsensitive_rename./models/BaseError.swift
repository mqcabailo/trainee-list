//
//  BaseError.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 01/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct BaseError: Error {
  var errorMessage: String 
}
