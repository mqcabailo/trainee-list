//
//  Token.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 14/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct Token {
  var accessToken: String
  var tokenType: String
  var expiresAt: String
}

// MARK: - Decodable
extension Token: Decodable { }

// MARK: - Coding Keys
extension Token {
  private enum CodingKeys: String, CodingKey {
    case accessToken = "access_token"
    case tokenType = "token_type"
    case expiresAt = "expires_at"
  }
}
