//
//  Trainee.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Trainee: Object, Decodable {
  @objc dynamic var id: Int = 0
  @objc dynamic var name: String = ""
  @objc dynamic var email: String = ""
  @objc dynamic var group: String = ""
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  init(id: Int, name: String, email: String, group: String) {
    self.id = id
    self.name = name
    self.email = email
    self.group = group
    
    super.init()
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decode(Int.self, forKey: .id)
    name = try container.decode(String.self, forKey: .name)
    email = try container.decode(String.self, forKey: .email)
    group = try container.decode(String.self, forKey: .group)
    
    super.init()
  }
  
  required init(realm: RLMRealm, schema: RLMObjectSchema) {
    super.init(realm: realm, schema: schema)
  }
  
  required init(value: Any, schema: RLMSchema) {
    super.init(value: value, schema: schema)
  }
  
  required init() {
    super.init()
  }
}

// MARK: - Encodable
extension Trainee: Encodable {
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(name, forKey: .name)
    try container.encode(email, forKey: .email)
    try container.encode(group, forKey: .group)
  }
}

// MARK: - Coding Keys
extension Trainee {
  private enum CodingKeys: String, CodingKey {
    case id = "id"
    case name = "name"
    case email = "email"
    case group = "group"
  }
}
