//
//  SignupUser.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct SignupUser {
  public let name: String
  public let email: String
  public let password: String
  public let confirmPassword: String
}

// MARK: - Encodable
extension SignupUser: Encodable {
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(name, forKey: .name)
    try container.encode(email, forKey: .email)
    try container.encode(password, forKey: .password)
    try container.encode(confirmPassword, forKey: .confirmPassword)
  }
}

// MARK: - Coding Keys
extension SignupUser {
  private enum CodingKeys: String, CodingKey {
    case name = "name"
    case email = "email"
    case password = "password"
    case confirmPassword = "password_confirmation"
  }
}
