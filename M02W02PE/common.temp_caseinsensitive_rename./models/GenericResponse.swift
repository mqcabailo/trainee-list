//
//  GenericResponse.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 12/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

class GenericResponse<Element: Decodable>: Decodable {
  var data: Element
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    data = try container.decode(Element.self, forKey: .data)
  }  
}

// MARK: - Coding Keys
extension GenericResponse {
  private enum CodingKeys: String, CodingKey {
    case data = "data"
  }
}
