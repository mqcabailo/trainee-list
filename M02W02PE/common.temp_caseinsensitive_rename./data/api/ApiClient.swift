//
//  ApiClient.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 07/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {
  
  static func login(loginData: Data,
                    completion: @escaping (Result<Data?, BaseError>) -> Void) {
    return request(ApiRouter.login(loginData: loginData), completion)
  }
  
  static func signup(signupData: Data,
                     completion: @escaping (Result<Data?, BaseError>) -> Void) {
    return request(ApiRouter.signup(signupData: signupData), completion)
  }
  
  static func storeTrainee(traineeData: Data,
                           completion: @escaping (Result<Data?, BaseError>) -> Void) {
    return request(ApiRouter.storeTrainee(traineeData: traineeData), completion)
  }
  
  static func updateTrainee(id: Int, traineeData: Data,
                            completion: @escaping (Result<Data?, BaseError>) -> Void) {
    return request(ApiRouter.updateTrainee(id: id, traineeData: traineeData), completion)
  }
  
  static func getAllTrainees(group: String,
                             completion: @escaping (Result<Data?, BaseError>) -> Void) {
    return request(ApiRouter.getAllTrainees(group: group), completion)
  }
  
  static func deleteTrainee(id: Int,
                            completion: @escaping (Result<Data?, BaseError>) -> Void) {
    return request(ApiRouter.deleteTrainee(id: id), completion)
  }
  
  private static func request(_ urlConvertible: URLRequestConvertible,
                              _ completion: @escaping (Result<Data?, BaseError>) -> Void) {
    if NetworkReachabilityManager()?.isReachable ?? false {
      AF.request(urlConvertible)
        .validate()
        .responseJSON { (response: DataResponse<Any, AFError>) in
          switch response.result {
          case .success:
            completion(Result.success(response.data))
          case .failure:
            if let statusCode = response.response?.statusCode {
              var errorMessage: String {
                switch statusCode {
                case 401: return "API: unauthorized"
                case 403: return "API: forbidden"
                case 404: return "API: not found"
                case 409: return "API: conflict"
                case 500: return "API: internal server error"
                default: return "API: unknown"
                }
              }
              completion(Result.failure(BaseError(errorMessage: errorMessage)))
            }
          }
      }
    } else {
      completion(Result.failure(BaseError(errorMessage: "No internet connection found")))
    }
  }
}
