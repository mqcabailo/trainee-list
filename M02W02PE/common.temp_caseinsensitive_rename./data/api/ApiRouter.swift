//
//  ApiRouter.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 07/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRouter: URLRequestConvertible {
  case login(loginData: Data)
  case signup(signupData: Data)
  case storeTrainee(traineeData: Data)
  case updateTrainee(id: Int, traineeData: Data)
  case getAllTrainees(group: String)
  case deleteTrainee(id: Int)
  
  func asURLRequest() throws -> URLRequest {
    var url = try Constants.baseUrl.asURL()
    url = url.appendingPathComponent(path)
    var urlRequest = URLRequest(url: url)
    urlRequest.method = method
    urlRequest.headers = headers
    urlRequest.httpBody = body
    urlRequest = try encoding.encode(urlRequest, with: parameters)
    
    return urlRequest
  }
  
  // MARK: - HTTPMethod
  private var method: HTTPMethod {
    switch self {
    case .login, .signup, .storeTrainee:
      return .post
    case .updateTrainee:
      return .put
    case .getAllTrainees:
      return .get
    case .deleteTrainee:
      return .delete
    }
  }
  
  // MARK: - HTTPHeaders
  private var headers: HTTPHeaders {
    switch self {
    case .login, .signup:
      return [
        Constants.HttpHeaderField.acceptType.rawValue: Constants.ContentType.json.rawValue,
        Constants.HttpHeaderField.contentType.rawValue: Constants.ContentType.json.rawValue
      ]
    default:
      return [
        Constants.HttpHeaderField.acceptType.rawValue: Constants.ContentType.json.rawValue,
        Constants.HttpHeaderField.contentType.rawValue: Constants.ContentType.json.rawValue,
        Constants.HttpHeaderField.authorization.rawValue: "\(UserDefaults.standard.string(forKey: "TokenType") ?? "")  \(UserDefaults.standard.string(forKey: "AccessToken") ?? "")"
      ]
    }
  }
  
  // MARK: - Encoding
  private var encoding: ParameterEncoding {
    switch method {
    case .get:
      return URLEncoding.default
    default:
      return JSONEncoding.default
    }
  }
  
  // MARK: - Path
  private var path: String {
    switch self {
    case .login:
      return "api/auth/login"
    case .signup:
      return "api/auth/signup"
    case .storeTrainee:
      return "api/trainees"
    case .updateTrainee(let id, _):
      return "api/trainees/\(id)"
    case .getAllTrainees:
      return "api/trainees"
    case .deleteTrainee(let id):
      return "api/trainees/\(id)"
    }
  }
  
  // MARK: - Parameters
  private var parameters: Parameters? {
    switch self {
    case .getAllTrainees(let group):
      return ["group": group]
    default:
      return nil
    }
  }
  
  // MARK: - Body
  private var body: Data? {
    switch self {
    case .login(let loginData):
      return loginData
    case .signup(let signupData):
      return signupData
    case .storeTrainee(let traineeData):
      return traineeData
    case .updateTrainee(_, let traineeData):
      return traineeData
    default:
      return nil
    }
  }
}
