//
//  RealmError.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 01/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

struct RealmError: Error {
  public enum ErrorTypes: String {
    case fileAccess = "Realm: Another process is using the database file."
    case filePermissionDenied =  "Realm: Denied access to database file."
    case fileExists = "Realm: Database file already exists."
    case fileNotFound = "Realm: Database file cannot be found."
    case unknown = "Realm: unknown error"
  }
  var errorType: ErrorTypes
  var errorMessage: String {
    return errorType.rawValue
  }
}
