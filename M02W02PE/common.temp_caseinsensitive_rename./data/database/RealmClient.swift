//
//  RealmClient.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 04/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class RealmClient {
  private enum ErrorMessages {
    static let fileAccess = "Realm: Another process is using the database file."
    static let filePermissionDenied =  "Realm: Denied access to database file."
    static let fileExists = "Realm: Database file already exists."
    static let fileNotFound = "Realm: Database file cannot be found."
    static let unknown = "Realm: unknown error"
  }
  
  static func setRealmForUser() {
    var config = Realm.Configuration()
    config.fileURL = config.fileURL!
      .deletingLastPathComponent()
      .appendingPathComponent("\(UserDefaults.standard.string(forKey: "email") ?? "default").realm")
    Realm.Configuration.defaultConfiguration = config
  }
  
  static func deleteRealmForUser(completion: @escaping (Result<Bool, BaseError>) -> Void) {
    do {
      let realm = try Realm()
      try realm.write {
        realm.deleteAll()
      }
      completion(Result.success(true))
    } catch {
      completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
    }
  }
  
  static func getAllTrainees(group: String, completion: @escaping (Result<[Trainee], BaseError>) -> Void) {
    do {
      let realm = try Realm()
      let predicate = NSPredicate(format: "group = %@", group)
      let results = realm.objects(Trainee.self).filter(predicate)
      var trainees: [Trainee] = []
      
      for trainee in results {
        trainees.append(trainee)
      }
      
      completion(Result.success(trainees))
    } catch Realm.Error.fileAccess {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileAccess)))
    } catch Realm.Error.filePermissionDenied {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.filePermissionDenied)))
    } catch Realm.Error.fileExists {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileExists)))
    } catch Realm.Error.fileNotFound {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileNotFound)))
    } catch {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.unknown)))
    }
  }
  
  static func saveTraineesToLocal(trainees: [Trainee], completion: @escaping (Result<Bool, BaseError>) -> Void) {
    do {
      let realm = try Realm()
      try realm.write {
        realm.add(trainees, update: .modified)
      }
      
      completion(Result.success(true))
    } catch Realm.Error.fileAccess {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileAccess)))
    } catch Realm.Error.filePermissionDenied {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.filePermissionDenied)))
    } catch Realm.Error.fileExists {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileExists)))
    } catch Realm.Error.fileNotFound {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileNotFound)))
    } catch {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.unknown)))
    }
  }
  
  static func deleteTrainee(id: Int, completion: @escaping (Result<Bool, BaseError>) -> Void) {
    do {
      let realm = try Realm()
      let predicate = NSPredicate(format: "id = %d", id)
      let results = realm.objects(Trainee.self).filter(predicate)
      try realm.write {
        realm.delete(results)
      }
      completion(Result.success(true))
    } catch Realm.Error.fileAccess {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileAccess)))
    } catch Realm.Error.filePermissionDenied {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.filePermissionDenied)))
    } catch Realm.Error.fileExists {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileExists)))
    } catch Realm.Error.fileNotFound {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.fileNotFound)))
    } catch {
      completion(Result.failure(BaseError(errorMessage: ErrorMessages.unknown)))
    }
  }
}
