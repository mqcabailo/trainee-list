//
//  UserRepository.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 30/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation
import Alamofire

class UserRepository {
  private enum UserKeys {
    static let Email = "Email"
    static let AccessToken = "AccessToken"
    static let TokenType = "TokenType"
    static let ExpiresAt = "ExpiresAt"
  }
  
  func login(email: String, password: String, completion: @escaping (Result<Token, BaseError>) -> Void) {
    do {
      let user = User(email: email, password: password)
      let encodedData = try JSONEncoder().encode(user)
      
      ApiClient.login(loginData: encodedData) { (response) in
        switch response {
        case .success(let result):
          do {
            if let result = result {
              let decoder = JSONDecoder()
              let token = try decoder.decode(Token.self, from: result)
              UserDefaults.standard.set(email, forKey: UserKeys.Email)
              UserDefaults.standard.set(token.accessToken, forKey: UserKeys.AccessToken)
              UserDefaults.standard.set(token.tokenType, forKey: UserKeys.TokenType)
              UserDefaults.standard.set(token.expiresAt, forKey: UserKeys.ExpiresAt)
              RealmClient.setRealmForUser()
              completion(Result.success(token))
            }
          } catch {
            completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
          }
        case .failure(let error):
          completion(Result.failure(error))
        }
      }
    } catch {
      completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
    }
  }
  
  func signup(name: String, email: String, password: String,
              confirmPassword: String, completion: @escaping (Result<Bool, BaseError>) -> Void) {
    do {
      let signupUser = SignupUser(name: name, email: email, password: password, confirmPassword: confirmPassword)
      let encodedData = try JSONEncoder().encode(signupUser)
      
      ApiClient.signup(signupData: encodedData) { (response) in
        switch response {
        case .success:
          completion(Result.success(true))
        case .failure(let error):
          completion(Result.failure(BaseError(errorMessage: error.errorMessage)))
        }
      }
    } catch {
      completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
    }
  }
  
  func logout(completion: @escaping (Result<Bool, BaseError>) -> Void) {
    RealmClient.deleteRealmForUser { (response) in
      switch response {
      case .success:
        UserDefaults.standard.removeObject(forKey: UserKeys.Email)
        UserDefaults.standard.removeObject(forKey: UserKeys.AccessToken)
        UserDefaults.standard.removeObject(forKey: UserKeys.TokenType)
        UserDefaults.standard.removeObject(forKey: UserKeys.ExpiresAt)
        RealmClient.setRealmForUser()
        completion(Result.success(true))
      case .failure(let error):
        completion(Result.failure(error))
      }
    }
  }
  
  func sessionExists() -> Bool {
    guard UserDefaults.standard.string(forKey: UserKeys.Email) != nil,
      UserDefaults.standard.string(forKey: UserKeys.AccessToken) != nil,
      UserDefaults.standard.string(forKey: UserKeys.TokenType) != nil,
      UserDefaults.standard.string(forKey: UserKeys.ExpiresAt) != nil else {
      return false
    }
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let expiryDate = dateFormatter.date(from: UserDefaults.standard.string(forKey: UserKeys.ExpiresAt)!)!
    let currentDate = Date()
    
    if currentDate > expiryDate {
      logout(completion: { _ in })
      return false
    }
    
    return true
  }
}
