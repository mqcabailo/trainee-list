//
//  TraineeRepository.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 30/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class TraineeRepository {
  private var trainees: [Trainee] = []
  
  func getTraineesByGroup(group: String, completion: @escaping (Result<[Trainee], BaseError>) -> Void) {
    ApiClient.getAllTrainees(group: group) { (response) in
      switch response {
      case .success(let result):
        if let result = result {
          do {
            let decoder = JSONDecoder()
            let genericResponse = try decoder.decode(GenericResponse<[Trainee]>.self, from: result)
            self.trainees = genericResponse.data
            
            RealmClient.saveTraineesToLocal(trainees: self.trainees) { response in
              switch response {
              case .success:
                completion(Result.success(self.trainees))
              case .failure(let error):
                completion(Result.failure(error))
              }
            }
            
          } catch {
            completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
          }
        }
      case .failure:
        RealmClient.getAllTrainees(group: group) { (response) in
          switch response {
          case .success(let result):
            self.trainees = result
            completion(Result.success(self.trainees))
          case .failure(let error):
            completion(Result.failure(error))
          }
        }
      }
    }
  }
  
  func storeTrainee(name: String, email: String, group: String, completion: @escaping (Result<Bool, BaseError>) -> Void) {
    do {
      let trainee = Trainee(id: 0, name: name, email: email, group: group)
      let encodedData = try JSONEncoder().encode(trainee)
      ApiClient.storeTrainee(traineeData: encodedData) { (response) in
        switch response {
        case .success:
          completion(Result.success(true))
        case .failure(let error):
          completion(Result.failure(error))
        }
      }
    } catch {
      completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
    }
  }
  
  func updateTrainee(id: Int,
                     name: String,
                     email: String,
                     group: String,
                     completion: @escaping (Result<Bool, BaseError>) -> Void) {
    do {
      let trainee = Trainee(id: id, name: name, email: email, group: group)
      let encodedData = try JSONEncoder().encode(trainee)
      ApiClient.updateTrainee(id: id, traineeData: encodedData) { (response) in
        switch response {
        case .success:
          completion(Result.success(true))
        case .failure(let error):
          completion(Result.failure(error))
        }
      }
    } catch {
      completion(Result.failure(BaseError(errorMessage: error.localizedDescription)))
    }
  }
  
  func deleteTrainee(id: Int, completion: @escaping (Result<Bool, BaseError>) -> Void) {
    ApiClient.deleteTrainee(id: id) { (response) in
      switch response {
      case .success:
        RealmClient.deleteTrainee(id: id) { response in
          switch response {
          case .success:
            completion(Result.success(true))
          case .failure(let error):
            completion(Result.failure(error))
          }
        }
      case .failure(let error):
        completion(Result.failure(error))
      }
    }
  }
}
