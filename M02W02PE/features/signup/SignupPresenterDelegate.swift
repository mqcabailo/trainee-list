//
//  SignupPresenterDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol SignupPresenterDelegate {
  func signup(name: String, email: String, password: String, confirmPassword: String)
}
