//
//  SignupPresenter.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

// MARK: - Properties 
class SignupPresenter {
  private let userRepository: UserRepository
  private let signupViewDelegate: SignupViewDelegate
  
  init(signupViewDelegate: SignupViewController) {
    self.userRepository = UserRepository()
    self.signupViewDelegate = signupViewDelegate
  }
}

// MARK: - Methods
extension SignupPresenter {
  private func validateInput(name: String, email: String, password: String, confirmPassword: String, completion: (Result<Bool, BaseError>) -> Void) {
    let validName = FieldValidations.validate(input: name, fieldType: .name, isRequired: true)
    let validEmail = FieldValidations.validate(input: email, fieldType: .email, isRequired: true)
    var valid = true
    var message = ""
    
    if !validName.isValid {
      valid = false
      message = "\(message)\(validName.message)\n"
    }
    
    if !validEmail.isValid {
      valid = false
      message = "\(message)\(validEmail.message)\n"
    }
    
    if password != confirmPassword {
      valid = false
      message = "\(message)Passwords do not match\n"
    }
    
    if valid {
      completion(Result.success(valid))
    } else {
      completion(Result.failure(BaseError(errorMessage: message)))
    }
  }
}

// MARK: - SignupPresenterDelegate
extension SignupPresenter: SignupPresenterDelegate {
  func signup(name: String, email: String, password: String, confirmPassword: String) {
    signupViewDelegate.showProgress()
    validateInput(name: name, email: email, password: password, confirmPassword: confirmPassword) { (response) in
      self.signupViewDelegate.hideProgress()
      switch response {
      case .success:
        self.signupViewDelegate.showSuccess()
        self.signupViewDelegate.clearInput()
      case .failure(let error):
        self.signupViewDelegate.showErrorMessage(errorMessage: error.errorMessage)
      }
    }
  }
}
