//
//  SignupViewDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol SignupViewDelegate {
  func showProgress()
  func hideProgress()
  func showErrorMessage(errorMessage: String)
  func showSuccess()
  func clearInput()
  func navigateToPrevious()
}
