//
//  SignupViewController.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 02/10/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

// MARK: - Properties
class SignupViewController: UIViewController {
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var confirmPasswordTextField: UITextField!
  private var signUpPresenter: SignupPresenterDelegate!
}

// MARK: - Life Cycle
extension SignupViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    signUpPresenter = SignupPresenter(signupViewDelegate: self)
  }
}

// MARK: - IBActions
extension SignupViewController {
  @IBAction func touchCreateAccount(_ sender: UIButton) {
    if let name = nameTextField.text,
      let email = emailTextField.text,
      let password = passwordTextField.text,
      let confirmPassword = confirmPasswordTextField.text {
      
      signUpPresenter.signup(name: name, email: email, password: password, confirmPassword: confirmPassword)
    }
  }
  
  @IBAction func touchCloseButton(_ sender: UIBarButtonItem) {
    navigateToPrevious()
  }
}

// MARK: - SignUpViewDelegate
extension SignupViewController: SignupViewDelegate {
  func showProgress() {
    
  }
  
  func hideProgress() {
    
  }
  
  func showErrorMessage(errorMessage: String) {
    let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func showSuccess() {
    let alert = UIAlertController(title: "Success", message: "Signup successful", preferredStyle: .alert)
    let action = UIAlertAction(title: "Okay", style: .default, handler: nil)
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func clearInput() {
    nameTextField.text = ""
    emailTextField.text = ""
    passwordTextField.text = ""
    confirmPasswordTextField.text = ""
  }
  
  func navigateToPrevious() {
    dismiss(animated: true, completion: nil)
  }
}
