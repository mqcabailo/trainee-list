//
//  TraineeListViewDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 04/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol TraineeListViewDelegate {
  func showProgress()
  func hideProgress()
  func showLogoutAlert(callback: @escaping (Bool) -> Void)
  func showErrorMessage(_ errorMessage: String)
  func refreshTrainees(trainees: [Trainee])
  func navigateToLogin()
}
