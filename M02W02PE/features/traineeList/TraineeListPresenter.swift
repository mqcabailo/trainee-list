//
//  TraineeListPresenter.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 04/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

// MARK: - Properties
class TraineeListPresenter {
  private let traineeListView: TraineeListViewDelegate
  private let traineeRepository: TraineeRepository
  private let userRepository: UserRepository
  
  init(traineeListView: TraineeListViewController) {
    self.traineeListView = traineeListView
    self.traineeRepository = TraineeRepository()
    self.userRepository = UserRepository()
  }
}

// MARK: - TraineeListPresenterDelegate
extension TraineeListPresenter: TraineeListPresenterDelegate {
  func logout() {
    traineeListView.showLogoutAlert { (confirmed) in
      if confirmed {
        self.userRepository.logout { (response) in
          switch response {
          case .success:
            self.traineeListView.navigateToLogin()
          case .failure(let error):
            self.traineeListView.showErrorMessage(error.localizedDescription)
          }
        }
      }
    }
  }
  
  func getAllTrainees(group: String) {
    traineeListView.showProgress()
    traineeRepository.getTraineesByGroup(group: group) { (response) in
      switch response {
      case .success(let result):
        self.traineeListView.refreshTrainees(trainees: result)
      case .failure(let error):
        self.traineeListView.showErrorMessage(error.localizedDescription)
      }
      self.traineeListView.hideProgress()
    }
  }
}
