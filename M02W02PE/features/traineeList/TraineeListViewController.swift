//
//  TraineeListViewController.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 05/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

// MARK: - Properties
class TraineeListViewController: UIViewController {
  @IBOutlet weak var traineeTableView: UITableView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  private var traineeListPresenterDelegate: TraineeListPresenterDelegate!
  private var trainees: [Trainee] = []
  private enum CellIdentifiers {
    static let TraineeTableViewCell = "TraineeTableViewCell"
  }
  private enum SegueIdentifiers {
    static let ShowStoreTrainee = "ShowStoreTrainee"
    static let ShowTraineeDetails = "ShowTraineeDetails"
  }
}

// MARK: - Life Cycle
extension TraineeListViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    traineeListPresenterDelegate = TraineeListPresenter(traineeListView: self)
    traineeTableView.delegate = self
    traineeTableView.dataSource = self
    traineeTableView.register(
      UINib(nibName: "TraineeTableViewCell", bundle: nil),
      forCellReuseIdentifier: CellIdentifiers.TraineeTableViewCell)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    if let group = navigationController?.tabBarItem.title {
      traineeListPresenterDelegate.getAllTrainees(group: group)
    }
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    trainees.removeAll()
    traineeTableView.reloadData()
  }
}

// MARK: - Navigation
extension TraineeListViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == SegueIdentifiers.ShowTraineeDetails {
      if let viewController = segue.destination as? TraineeDetailsViewController,
        let indexPath = traineeTableView.indexPathForSelectedRow {
        viewController.trainee = trainees[indexPath.row]
      }
    }
  }
}

// MARK: - IBActions
extension TraineeListViewController {
  @IBAction func touchLogoutButton(_ sender: Any) {
    traineeListPresenterDelegate.logout()
  }
  
  @IBAction func touchAddButton(_ sender: UIBarButtonItem) {
    performSegue(withIdentifier: SegueIdentifiers.ShowStoreTrainee, sender: nil)
  }
}

// MARK: - TraineeListViewDelegate
extension TraineeListViewController: TraineeListViewDelegate {
  func showProgress() {
    activityIndicator.startAnimating()
  }
  
  func hideProgress() {
    activityIndicator.stopAnimating()
  }
  
  func showLogoutAlert(callback: @escaping (Bool) -> Void) {
    let alert = UIAlertController(title: "Hang on", message: "Are you sure you want to logout?", preferredStyle: .alert)
    let confirm = UIAlertAction(title: "YES", style: .default) { _ in
      callback(true)
    }
    let cancel  = UIAlertAction(title: "CANCEL", style: .cancel) { _ in
      callback(false)
    }
    alert.addAction(confirm)
    alert.addAction(cancel)
    present(alert, animated: true)
  }
  
  func showErrorMessage(_ errorMessage: String) {
    let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func refreshTrainees(trainees: [Trainee]) {
    self.trainees = trainees
    traineeTableView.reloadData()
  }
  
  func navigateToLogin() {
    dismiss(animated: true, completion: nil)
  }
}

// MARK: - UITableViewDelegate
extension TraineeListViewController: UITableViewDelegate {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return trainees.count
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    performSegue(withIdentifier: SegueIdentifiers.ShowTraineeDetails, sender: nil)
  }
}

// MARK: - UITableViewDataSource
extension TraineeListViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(
      withIdentifier: CellIdentifiers.TraineeTableViewCell,
      for: indexPath) as! TraineeTableViewCell
    cell.fullNameLabel.text = trainees[indexPath.row].name
    cell.emailLabel.text = trainees[indexPath.row].email
    
    return cell
  }
}
