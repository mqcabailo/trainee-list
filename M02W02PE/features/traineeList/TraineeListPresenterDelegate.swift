//
//  TraineeListProtocol.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 04/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol TraineeListPresenterDelegate {
  func logout()
  func getAllTrainees(group: String)
}
