//
//  TraineeDetailsPresenter.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 05/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

// MARK: - Properties
class TraineeDetailsPresenter {
  private let traineeDetailsView: TraineeDetailsViewDelegate
  private let traineeRepository: TraineeRepository
  
  init(traineeDetailsView: TraineeDetailsViewController) {
    self.traineeDetailsView = traineeDetailsView
    self.traineeRepository = TraineeRepository()
  }
}

// MARK: - Methods
extension TraineeDetailsPresenter {
  private func validateInput(name: String, email: String, group: String) -> (isValid: Bool, errorMessage: String) {
    let validNameFormat = FieldValidations.validate(input: name, fieldType: .name, isRequired: true)
    let validEmailFormat = FieldValidations.validate(input: email, fieldType: .email, isRequired: true)
    var valid: Bool = true
    var message: String = ""
    
    if !validNameFormat.isValid {
      valid = false
      message = "\(message)\(validNameFormat.message)\n"
    }
    
    if !validEmailFormat.isValid {
      valid = false
      message = "\(message)\(validEmailFormat.message)\n"
    }
    
    if group.isEmpty {
      valid = false
      message = "\(message)Group must not be empty.\n"
    }
    
    return (valid, message)
  }
}

// MARK: - TraineeDetailsPresenterDelegate
extension TraineeDetailsPresenter: TraineeDetailsPresenterDelegate {
  func updateTrainee(havingId id: Int, name: String, email: String, group: String) {
    let validationResult = validateInput(name: name, email: email, group: group)
    if validationResult.isValid {
      traineeDetailsView.showUpdateAlert { (confirmed) in
        if confirmed {
          self.traineeDetailsView.showProgress()
          self.traineeRepository.updateTrainee(
            id: id, name: name,email: email,
            group: group) { (response) in
              switch response {
              case .success:
                self.traineeDetailsView.hideProgress()
                self.traineeDetailsView.showUpdateSuccess(callback: {})
              case .failure(let error):
                self.traineeDetailsView.hideProgress()
                self.traineeDetailsView.showErrorMessage(error.localizedDescription)
              }
          }
        }
      }
    } else {
      traineeDetailsView.showErrorMessage(validationResult.errorMessage)
    }
  }
  
  func deleteTrainee(havingId id: Int) {
    traineeDetailsView.showDeleteAlert { (confirmed) in
      if confirmed {
        self.traineeDetailsView.showProgress()
        self.traineeRepository.deleteTrainee(id: id) { (response) in
          switch response {
          case .success:
            self.traineeDetailsView.hideProgress()
            self.traineeDetailsView.showDeleteSuccess {
              self.traineeDetailsView.navigateToPrevious()
            }
          case .failure(let error):
            self.traineeDetailsView.hideProgress()
            self.traineeDetailsView.showErrorMessage(error.localizedDescription)
          }
        }
      }
    }
  }
}
