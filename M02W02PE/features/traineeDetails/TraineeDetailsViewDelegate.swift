//
//  TraineeDetailsViewDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 05/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol TraineeDetailsViewDelegate {
  func showProgress()
  func hideProgress()
  func showUpdateAlert(callback: @escaping (Bool) -> Void)
  func showDeleteAlert(callback: @escaping (Bool) -> Void)
  func showUpdateSuccess(callback: @escaping () -> Void)
  func showDeleteSuccess(callback: @escaping () -> Void)
  func showErrorMessage(_ errorMessage: String)
  func navigateToPrevious()
}
