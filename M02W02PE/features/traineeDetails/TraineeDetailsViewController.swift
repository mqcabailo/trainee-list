//
//  TraineeDetailsViewController.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 16/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

// MARK: - Properties
class TraineeDetailsViewController: UIViewController {
  @IBOutlet weak var traineeDetailsCustomView: TraineeDetailsView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  private var traineeDetailsPresenterDelegate: TraineeDetailsPresenterDelegate!
  var trainee: Trainee?
}

// MARK: - Life Cycle
extension TraineeDetailsViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    if let trainee = trainee {
      traineeDetailsCustomView.nameTextField.text = trainee.name
      traineeDetailsCustomView.emailTextField.text = trainee.email
      traineeDetailsCustomView.platformTextField.text = trainee.group
    }
    traineeDetailsPresenterDelegate = TraineeDetailsPresenter(traineeDetailsView: self)
    traineeDetailsCustomView.endButtonEvent = UITapGestureRecognizer(
      target: self,
      action: #selector (updateTrainee(_:)))
  }
}

// MARK: - IBActions
extension TraineeDetailsViewController {
  @IBAction func touchDeleteTrainee(_ sender: UIBarButtonItem) {
    if let trainee = trainee {
      traineeDetailsPresenterDelegate.deleteTrainee(havingId: trainee.id)
    }
  }
}

// MARK: - Methods
extension TraineeDetailsViewController {
  @objc private func updateTrainee(_ sender: UITapGestureRecognizer) {
    if let name = traineeDetailsCustomView.nameTextField.text,
      let email = traineeDetailsCustomView.emailTextField.text,
      let group = traineeDetailsCustomView.platformTextField.text,
      let trainee = trainee {
      traineeDetailsPresenterDelegate.updateTrainee(
        havingId: trainee.id,
        name: name,
        email: email,
        group: group)
    }
  }
}

// MARK: - TraineeDetailesViewDelegate
extension TraineeDetailsViewController: TraineeDetailsViewDelegate {
  func showProgress() {
    activityIndicator.startAnimating()
  }
  
  func hideProgress() {
    activityIndicator.stopAnimating()
  }
  
  func showUpdateAlert(callback: @escaping (Bool) -> Void) {
    let alert = UIAlertController(
      title: "Confirm Update",
      message: "Are you sure you want to update trainee?",
      preferredStyle: .actionSheet)
    let confirm = UIAlertAction(title: "Update", style: .default) { _ in
      callback(true)
    }
    let cancel  = UIAlertAction(title: "Cancel", style: .cancel) { _ in
      callback(false)
    }
    alert.addAction(confirm)
    alert.addAction(cancel)
    present(alert, animated: true)
  }
  
  func showDeleteAlert(callback: @escaping (Bool) -> Void) {
    let alert = UIAlertController(
      title: "Confirm Delete",
      message: "Are you sure you want to delete trainee?",
      preferredStyle: .actionSheet)
    let confirm = UIAlertAction(title: "Delete", style: .destructive) { _ in
      callback(true)
    }
    let cancel  = UIAlertAction(title: "Cancel", style: .cancel) { _ in
      callback(false)
    }
    alert.addAction(confirm)
    alert.addAction(cancel)
    present(alert, animated: true)
  }
  
  func showUpdateSuccess(callback: @escaping () -> Void) {
    let alert   = UIAlertController(title: "Updated Trainee", message: nil, preferredStyle: .alert)
    let action  = UIAlertAction(title: "Close", style: .default) { _ in
      callback()
    }
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func showDeleteSuccess(callback: @escaping () -> Void) {
    let alert = UIAlertController(title: "Deleted Trainee", message: nil, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .default) { _ in
      callback()
    }
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func showErrorMessage(_ errorMessage: String) {
    let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func navigateToPrevious() {
    navigationController?.popViewController(animated: true)
  }
}
