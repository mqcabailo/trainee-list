//
//  StoreTraineeViewController.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 15/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

// MARK: - Properties
class StoreTraineeViewController: UIViewController {
  @IBOutlet weak var traineeDetailsCustomView: TraineeDetailsView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  private var storeTraineePresenterDelegate: StoreTraineePresenterDelegate!
}

// MARK: - Life Cycle
extension StoreTraineeViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    storeTraineePresenterDelegate = StoreTraineePresenter(storeTraineeView: self)
    traineeDetailsCustomView.endButtonEvent = UITapGestureRecognizer(target: self, action: #selector (addTrainee(_:)))
  }
}

// MARK: - Methods
extension StoreTraineeViewController {
  @objc private func addTrainee(_ sender: UITapGestureRecognizer) {
    if let name = traineeDetailsCustomView.nameTextField.text,
      let email = traineeDetailsCustomView.emailTextField.text,
      let group = traineeDetailsCustomView.platformTextField.text {
      storeTraineePresenterDelegate.storeTrainee(name: name, email: email, group: group)
    }
  }
}

// MARK: - AddTraineeViewDelegate
extension StoreTraineeViewController: StoreTraineeViewDelegate {
  func showProgress() {
    activityIndicator.startAnimating()
  }
  
  func hideProgress() {
    activityIndicator.stopAnimating()
  }
  
  func showErrorMessage(_ errorMessage: String) {
    let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func showStoreTraineeSuccess(callback: @escaping () -> Void) {
    let alert = UIAlertController(title: "Stored Trainee", message: nil, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .cancel) { _ in
      callback()
    }
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func navigateToPrevious() {
    navigationController?.popViewController(animated: true)
  }
}
