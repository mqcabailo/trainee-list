//
//  StoreTraineeViewDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 05/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol StoreTraineeViewDelegate {
  func showProgress()
  func hideProgress()
  func showErrorMessage(_ errorMessage: String)
  func showStoreTraineeSuccess(callback: @escaping () -> Void)
  func navigateToPrevious()
}
