//
//  StoreTraineePresenterDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 05/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol StoreTraineePresenterDelegate {
  func storeTrainee(name: String, email: String, group: String)
}
