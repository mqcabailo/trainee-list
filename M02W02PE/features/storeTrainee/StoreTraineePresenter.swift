//
//  StoreTraineePresenter.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 05/09/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

// MARK: - Properties
class StoreTraineePresenter {
  private let storeTraineeView: StoreTraineeViewDelegate
  private let traineeRepository: TraineeRepository
  
  init(storeTraineeView: StoreTraineeViewController) {
    self.storeTraineeView = storeTraineeView
    self.traineeRepository = TraineeRepository()
  }
}

// MARK: - Methods
extension StoreTraineePresenter {
  private func validateInput(name: String, email: String,
                             group: String, completion: (Result<Bool, BaseError>) -> Void) {
    let validNameFormat = FieldValidations.validate(input: name, fieldType: .name, isRequired: true)
    let validEmailFormat = FieldValidations.validate(input: email, fieldType: .email, isRequired: true)
    var valid = true
    var message = ""
    
    if !validNameFormat.isValid {
      valid = false
      message = "\(message)\(validNameFormat.message)\n"
    }
    
    if !validEmailFormat.isValid {
      valid = false
      message = "\(message)\(validEmailFormat.message)\n"
    }
    
    if group.isEmpty {
      valid = false
      message = "\(message)Group must not be empty.\n"
    }
    
    if valid {
      completion(Result.success(valid))
    } else {
      completion(Result.failure(BaseError(errorMessage: message)))
    }
  }
}

// MARK: - StoreTraineePresenterDelegate
extension StoreTraineePresenter: StoreTraineePresenterDelegate {
  func storeTrainee(name: String, email: String, group: String) {
    validateInput(name: name, email: email, group: group) { (response) in
      switch response {
      case .success:
        storeTraineeView.showProgress()
        traineeRepository.storeTrainee(name: name, email: email, group: group) { (response) in
          switch response {
          case .success:
            self.storeTraineeView.showStoreTraineeSuccess {
              self.storeTraineeView.navigateToPrevious()
            }
          case .failure(let error):
            self.storeTraineeView.showErrorMessage(error.localizedDescription)
          }
        }
        storeTraineeView.hideProgress()
      case .failure(let error):
        storeTraineeView.showErrorMessage(error.errorMessage)
      }
    }
  }
}
