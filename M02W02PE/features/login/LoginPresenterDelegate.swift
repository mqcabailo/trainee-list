//
//  LoginPresenterProtocol.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 30/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol LoginPresenterDelegate {
  func login(email: String, password: String)
  func checkSession()
  func checkInputContent(email: String, password: String)
}
