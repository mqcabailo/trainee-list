//
//  LoginViewController.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 15/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import UIKit

// MARK: - Properties
class LoginViewController: UIViewController {
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  private var loginPresenter: LoginPresenterDelegate!
  private enum SegueIdentifier {
    static let ShowTraineeList = "ShowTraineeList"
    static let ShowSignup = "ShowSignup"
  }
}

// MARK: - Life Cycle
extension LoginViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    loginPresenter = LoginPresenter(loginViewDelegate: self)
    emailTextField.delegate = self
    passwordTextField.delegate = self
  }
  
  override func viewDidAppear(_ animated: Bool) {
    loginPresenter.checkSession()
  }
}

// MARK: - IBActions
extension LoginViewController {
  @IBAction func touchLoginButton(_ sender: Any) {
    if let email = emailTextField.text, let password = passwordTextField.text {
      loginPresenter.login(email: email, password: password)
    }
  }
  
  @IBAction func touchSignup(_ sender: UIButton) {
    navigateToSignup()
  }
  
  @IBAction func editingChangedEmail(_ sender: UITextField) {
    if let email = emailTextField.text,
      let password = passwordTextField.text {
      loginPresenter.checkInputContent(email: email, password: password)
    }
  }
  
  @IBAction func editingChangedPassword(_ sender: UITextField) {
    if let email = emailTextField.text,
      let password = passwordTextField.text {
      loginPresenter.checkInputContent(email: email, password: password)
    }
  }
}

// MARK: - LoginViewDelegate
extension LoginViewController: LoginViewDelegate {
  func showProgress() {
    activityIndicator.startAnimating()
  }
  
  func hideProgress() {
    activityIndicator.stopAnimating()
  }
  
  func showErrorMessage(errorMessage: String) {
    let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
    let action = UIAlertAction(title: "Close", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true)
  }
  
  func showLoginSuccess() {
    
  }
  
  func clearPassword() {
    passwordTextField.text = ""
  }
  
  func navigateToTraineeList() {
    performSegue(withIdentifier: SegueIdentifier.ShowTraineeList, sender: nil)
  }
  
  func navigateToSignup() {
    performSegue(withIdentifier: SegueIdentifier.ShowSignup, sender: nil)
  }
  
  func disableLogin() {
    loginButton.isEnabled = false
    loginButton.alpha = 0.75
  }
  
  func enableLogin() {
    loginButton.isEnabled = true
    loginButton.alpha = 1.00
  }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
      nextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
    }

    if textField == passwordTextField {
     if let email = emailTextField.text,
      let password = passwordTextField.text {
      loginPresenter.login(email: email, password: password)
      }
    }
    
    return false
  }
}
