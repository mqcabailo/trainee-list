//
//  LoginPresenter.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 30/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

// MARK: - Properties
class LoginPresenter {
  private let userRepository: UserRepository
  private let loginView: LoginViewDelegate
  
  init(loginViewDelegate: LoginViewController) {
    self.userRepository = UserRepository()
    self.loginView = loginViewDelegate
  }
}

// MARK: - Methods
extension LoginPresenter {
  private func validateInput(email: String, password: String, completion: (Result<Bool, BaseError>) -> Void) {
    let validEmailFormat = FieldValidations.validate(input: email, fieldType: .email, isRequired: true)
    let validPasswordFormat = FieldValidations.validate(input: password, fieldType: .password, isRequired: true)
    var valid = true
    var message = ""
    
    if !validEmailFormat.isValid {
      valid = false
      message = "\(message)\(validEmailFormat.message)\n"
    }
    
    if !validPasswordFormat.isValid {
      valid = false
      message = "\(message)\(validPasswordFormat.message)\n"
    }
    
    if valid {
      completion(Result.success(valid))
    } else {
      completion(Result.failure(BaseError(errorMessage: message)))
    }
  }
}

// MARK: - LoginPresenterDelegate
extension LoginPresenter: LoginPresenterDelegate {
  func login(email: String, password: String) {
    validateInput(email: email, password: password) { (response) in
      switch response {
      case .success:
        loginView.showProgress()
        userRepository.login(email: email, password: password) { (response) in
          self.loginView.hideProgress()
          switch response {
          case .success:
            self.loginView.clearPassword()
            self.loginView.showLoginSuccess()
            self.loginView.navigateToTraineeList()
          case .failure(let error):
            self.loginView.showErrorMessage(errorMessage: error.errorMessage)
          }
        }
      case .failure(let error):
        loginView.showErrorMessage(errorMessage: error.errorMessage)
      }
    }
  }
  
  func checkSession() {
    if userRepository.sessionExists() {
      loginView.showLoginSuccess()
      loginView.navigateToTraineeList()
    }
  }
  
  func checkInputContent(email: String, password: String) {
    if !email.isEmpty && !password.isEmpty {
      loginView.enableLogin()
    } else {
      loginView.disableLogin()
    }
  }
}
