//
//  LoginViewDelegate.swift
//  M02W02PE
//
//  Created by Matthew Cabailo on 30/08/2019.
//  Copyright © 2019 Matthew Cabailo. All rights reserved.
//

import Foundation

protocol LoginViewDelegate {
  func showProgress()
  func hideProgress()
  func showErrorMessage(errorMessage: String)
  func showLoginSuccess()
  func clearPassword()
  func navigateToTraineeList()
  func navigateToSignup()
  func enableLogin()
  func disableLogin()
}
